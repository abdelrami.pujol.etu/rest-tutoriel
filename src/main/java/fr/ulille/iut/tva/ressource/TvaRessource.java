package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.DetailsDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();
    
    
    
    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
            return TauxTva.NORMAL.taux;
    }

    
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
    	   try {
               return TauxTva.valueOf(niveau.toUpperCase()).taux; 
           }
           catch ( Exception ex ) {
               throw new NiveauTvaInexistantException();
           }
      
    }
    
    
    @GET
    @Path("{niveauTva}")
    public double getMontantTotal( @PathParam("niveauTva") String niveau, @QueryParam("prix") double somme) {
     
      try {
    	  return calculTva.calculerMontant(TauxTva.valueOf(niveau.toUpperCase()), somme);
      }
      catch ( Exception ex ) {
          throw new NiveauTvaInexistantException();
      }
    }
    
    
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }
    

//{"montantTotal":120.0,"montantTva":20.0,"somme":100.0,"tauxLabel":"NORMAL","tauxValue":20.0}
  //  Vous pouvez maintenant ajouter une méthode getDetail qui répondra à une URI de ce type :
    //	http://localhost:8080/api/v1/tva/details/{taux}?somme={valeur} 
    	//où taux correspond à un des niveaux de taux et valeur correspond à la somme utilisée pour le calcul.
    
    @GET
    @Path("details/{niveauTva}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DetailsDto getDetail( @PathParam("niveauTva") String niveau, @QueryParam("prix") double somme){
    	
    	DetailsDto d = new DetailsDto(
    			getMontantTotal(niveau, somme), 
    			getMontantTotal(niveau, somme)-somme, 
    			somme, 
    			TauxTva.valueOf(niveau.toUpperCase()).name(),
    			getValeurTaux(niveau));
    	
    	
    	//public DetailsDto(double montantTotal, double montantTva, double somme, String tauxLabel, double tauxValue)
    	
    	return d;
    	
    }
    
    
    
  
    
    
    
}
